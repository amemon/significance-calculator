from scipy import stats

ctv = 100
ctc = 15.0
vtv = 150.0
vtc = 60.0
controlpercentage = (ctc / ctv) * 100
variationpercentage = (vtc / vtv) * 100
sigdif = abs(controlpercentage - variationpercentage)

x2 = (((ctc-ctv) ** 2  ) / ctv) + (((vtc-vtv) **2 )/vtv)
x4 = (((ctv-ctc) ** 2  ) / ctc) + (((vtv-vtc) **2 )/vtc)
x3 = (((ctc-ctv)) / ctv) + (((vtc-vtv))/vtv)

p =  stats.chi2.pdf(x4 , 1)

print sigdif, controlpercentage, variationpercentage
print x2, x3, x4
print p
print round(p,2)
print type(p)