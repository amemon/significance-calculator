# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.


from django.shortcuts import render, get_object_or_404

from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.http import JsonResponse

from scipy import stats

def index(request):
	if request.method == 'GET':
		return render(request, 'index.html')
	if request.is_ajax():
		ctv = float(request.POST['ctv'])
		ctc = float(request.POST['ctc'])
		vtv = float(request.POST['vtv'])
		vtc = float(request.POST['vtc'])
		# Get Percentages
		controlpercentage = (ctc / ctv) * 100
		variationpercentage = (vtc / vtv) * 100

		# Get Difference between percentages
		sigdif = abs(controlpercentage - variationpercentage)

		# Calculate P Value
		x2 = (((ctv-ctc) ** 2  ) / ctc) + (((vtv-vtc) **2 )/vtc)
		p =  stats.chi2.pdf(x2 , 1)

		if sigdif >= 5:
			sig = 'Yes'
		else:
			sig = 'No'
		if str(p) == 'inf':
			p = None
		else:
			p = round(p)
		data = { 'significance' : sig, 'p' : p}
		return JsonResponse(data)